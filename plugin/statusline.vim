set statusline=                " Clear the status line
set statusline+=%-3.3n         " Buffer number
set statusline+=%f             " File name
set statusline+=%h%m%r%w       " Status flags
set statusline+=%=             " Right align remainder
set statusline+=%-14(%l,%c%V%) " Line, column
set statusline+=%<%P           " File position
